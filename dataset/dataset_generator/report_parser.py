import xml.etree.ElementTree
import os
import pandas as pd

class ReportParser:

    def __init__(self, xmlpath):
        self.xmlpath = xmlpath

    def _get_function_name_and_data_type(self, filename):
        if filename.endswith(".xml"):
            filename = os.path.join(self.xmlpath, filename)
            root = xml.etree.ElementTree.parse(filename).getroot()
            # Split Function name
            function_name = root.find('UserAssignments').find('TopModelName').text.rsplit(sep="_", maxsplit=1)[0]
            return function_name
    
    # Returns the function name and the data type  as a list of strings
    def _check_function_names(self):
        file_list = os.listdir(self.xmlpath)
        xml_list = [x for x in file_list if x.endswith(".xml")]
        ok = all(self._get_function_name_and_data_type(item) == self._get_function_name_and_data_type(xml_list[0]) for item in xml_list)
        print("OK, all functions are the same") if ok else print("Not ok")


    # Returns the dictionary with the number of resources used
    def extract_resources(self):
        res_df = pd.DataFrame()
        for filename in os.listdir(self.xmlpath):
            if filename.endswith(".xml"):
                self._check_function_names()
                root = xml.etree.ElementTree.parse(os.path.join(self.xmlpath, filename)).getroot()
                # Split Function name 'sum_int_1024' and take the last num
                function_name = root.find('UserAssignments').find('TopModelName').text.split(sep="_")

                res_dict = {}
                res_dict['num_operators'] = (int(function_name[2]))
                # Resources values
                res_node = root.find('AreaEstimates').find('Resources')

                for (el, name) in ('lut', 'LUT'), ('ff', 'FF'), ('dsp', 'DSP48E'), ('bram', 'BRAM_18K'):
                    res_dict[el] = (int(res_node.find(name).text))
                series = pd.Series(res_dict)
                res_df = res_df.append(series, ignore_index=True)

        return res_df.sort_values('num_operators')
