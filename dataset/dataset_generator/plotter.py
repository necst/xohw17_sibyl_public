import matplotlib.pyplot as plt
import os.path
import json

def plot(json_file):
    """Simple plotter showing resource in chart"""
    with open(json_file, 'r') as f:
        dict_spline = json.load(f)
        to_plot = dict_spline['array']['mul']['int']['ff']
        list_x = []
        list_y = []
        for (k,v) in to_plot.items():
            list_x.append(k)
            list_y.append(v)
        plt.scatter(list_x, list_y, s=2)
        plt.show()
        
plot('dataset.json')
