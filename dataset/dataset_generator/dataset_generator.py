#! /usr/bin/env python3
from scipy import interpolate
import click
import json
import os.path
from report_parser import ReportParser

@click.command()
@click.argument('path', nargs=1)
@click.option("--config-path", "-c",
              prompt="Enter the path for the configuration file",
              default="../config.json",
              help="Specify the path for the configuration file")

def find(path, config_path):
    """Simple program that make a spline interpolation for resource type"""
    with open(config_path, 'r') as json_config:
        config = json.load(json_config)
        data_types = config['data_types']
        operations = config['operations']
        values = config['values']
        scalar_array = config['scalar_array']

    spline_dict = _AutoVivification()
    for s_a in 'array', 'scalar':
        for op in operations:
            for dt in data_types:
                # Initialize variable at each loop iteration
                full_path = os.path.join(path, s_a, op, dt, 'data')
                rp = ReportParser(full_path)
                res_df = rp.extract_resources()
                # Extract spline interpolation
                for elem in 'lut', 'ff', 'dsp', 'bram':
                    v_x = list(res_df['num_operators'])
                    v_y = list(res_df[elem])
                    ret_spline = list(interpolate.spline(v_x, v_y, list(range(2,1028)), order = 1))
                    for i in range(2,1028):
                        spline_dict[s_a][op][dt][elem][i] = float(ret_spline[i-2])
                        #debug
                        print('running')
                        print(i)

    #store spline interpolation in json
    with open('dataset.json', 'w') as f:
        json.dump(spline_dict, f) 

#initialize a nested dict
class _AutoVivification(dict):
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

if __name__ == '__main__':
    find()
