#! /bin/bash
for op in add sub mul div
do
    cd ${op}
    for dt in int float double
    do
        cd ${dt}
        /opt/Xilinx/Vivado_HLS/2016.1/bin/vivado_hls -f script.tcl
        cd ../
    done
    cd ../
done

