#! /usr/bin/env python3

import click
import json
import os

class FunGenerator:

    def __init__(self, config, path):
        with open(config, 'r') as json_config:
            config = json.load(json_config)
        self.values = config['values']
        self.data_types = config['data_types']
        self.scalar_array = config['scalar_array']
        self.operations = config['operations']
        self.path = path

    def generate(self):
        directory = os.path.join(self.path, 'VivadoBench')
        os.makedirs(directory, exist_ok=True)
        for op in self.operations.keys():
            for s_a in self.scalar_array:
                for dt in self.data_types:
                    sub_directory = os.path.join(directory, s_a, op, dt)
                    os.makedirs(sub_directory, exist_ok=True)
                    # Open the source file for the associated function
                    c_file_path = os.path.join(sub_directory, "{}_{}_{}.cpp".format(op, s_a, dt))
                    with open(c_file_path, 'w') as f:
                        if s_a == 'scalar':
                            self._generate_c_file_scalar(f, op, dt)
                        else:
                            self._generate_c_file_array(f, op, dt)
                    f.close()

                    tcl_file_path = os.path.join(sub_directory, "script.tcl")
                    with open(tcl_file_path, 'w') as f:
                        self._generate_tcl_file(f, op, dt, s_a)
                    f.close()

    def _generate_c_file_scalar(self, c_file, operation, data_type):
        """C functions generator for scalar values, in form a1+a2+a3.."""
        for val in self.values:
            c_file.write(f"{data_type} {operation}_{data_type}_{val}(")
            [c_file.write(f"{data_type} a{el}, ") for el in range (1, val)]
            c_file.write(f" {data_type} a0){{\n{data_type} z=0;\n")
            c_file.write("z = a0")
            [c_file.write(f" {self.operations[operation]} a{i}") for i in range(1, val)]
            c_file.write(";\n")
            c_file.write("return z;\n")
            c_file.write("}\n")

    def _generate_c_file_array(self, c_file,  operation, data_type):
        """C functions generator for array values, in form a[1]+a[2]+a[3].."""
        for val in self.values:
            c_file.write(f"{data_type} {operation}_{data_type}_{val}({data_type} a[{val}]) {{\n")
            c_file.write(f"{data_type} z=0;\n")
            c_file.write("z = a[0]")
            [c_file.write(f" {self.operations[operation]} a[{i}]") for i in range(1, val)]
            c_file.write(";\n")
            c_file.write("return z;\n")
            c_file.write("}\n")

    def _generate_tcl_file(self, f, op, dt, s_a):
        """tcl script generator to run Vivado HLS using C functions generated using 'generate' method"""
        values = ""
        for v in self.values:
            values += str(v) + " "
        script = ( "open_project proj\n"
                  f"add_files {op}_{s_a}_{dt}.cpp\n"
                  f"set nums [list {values}]\n"
                   "foreach num $nums {\n"
                  f"  set_top {op}_{dt}_$num\n"
                   "  open_solution \"solution$num\"\n"
                   "  set_part {xc7z020clg484-1} -tool vivado\n"
                   "  create_clock -period 10 -name default\n"
                   "  csynth_design\n"
                   "}\n"
                   "exit") # Final exit to avoid being stuck in tcl shell
        f.write(script)

@click.command()
@click.option("--config", "-c",
              prompt="Enter the path for the configuration file",
              default="config.json",
              help="Specify the path for the configuration file")
@click.argument('path', nargs=1)

def generate(config, path):
    """Simple program that generates C functions"""
    fg = FunGenerator(config, path)
    fg.generate()

if __name__ == '__main__':
    generate()

