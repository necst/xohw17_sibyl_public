import click
import os
from ir_parser import IRParser
import json
import pandas

def estimate(parser_df):
    
    res_dict = {'lut' : 0,
                'ff' : 0,
                'dsp' : 0,
                'bram': 0 }

    with open('dataset.json', 'r') as f:
       spline = json.load(f)

    # Find all types of operations
    for i, op in parser_df.drop_duplicates('operation')['operation'].iteritems():

        # Find all datatypes
        for a, dt in parser_df.drop_duplicates('datatype')['datatype'].iteritems():

            # Build a DF of all vectors withouth dependencies
            df_vector_type = parser_df.loc[(parser_df['vector_length'] != 1)
                & (parser_df['dependencies'].str.len() == 0)
                & (parser_df['datatype'] == dt)
                & (parser_df['operation'] == op)]
            # Number of vectors without dependencies extracted
            leng_vector_type = len(df_vector_type.index)

            # Get the number of vectorial operations
            # (Sum all parallel operations, multiply them by 2 ?
            # and multiply by the number of vectors found)
            vector_count = int(df_vector_type['vector_length'].sum()
                    * 2 * leng_vector_type)

            # Get the number of non vectorial operations without dependencies
            scalar_count = (len(parser_df.loc[(parser_df['vector_length'] == 1)
                & (parser_df['dependencies'].str.len() == 0)
                & (parser_df['datatype'] == dt)
                & (parser_df['operation'] == op)].index)) + 1

            # Find the usage of each type of resource using splines
            for res in res_dict:
                if(op != 'div'):
                    if scalar_count !=1 or vector_count:
                        tot = scalar_count + vector_count
                        res_dict[res] += (spline['array'][op][dt][res]['{}'
                            .format(int(tot))])
    print()
    for k, v in res_dict.items():
        print("{}: {}".format(k.upper(),int(v)))
    print()



@click.command()
@click.option("--llvm-ir", "-l",
              prompt="Enter the path for the LLVM IR file",
              help="Specify the path for the LLVM IR file")
@click.option("--top-function", "-t",
              prompt="Enter the name of the top function",
              help="Specify the name of the top function")

def run_estimation(llvm_ir, top_function):
    """Run the estimation of hardware resources usage"""
    parser = IRParser()
    df = parser.parse(llvm_ir, top_function)
    print()
    print("Hardware Resources Estimation for {} using {}"
          + " as top function".format(llvm_ir, top_function))
    estimate(df)

if __name__ == '__main__':
    run_estimation()
