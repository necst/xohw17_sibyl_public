import pandas as pd
from llvmcpy.llvm import *

class IRParser():

    def __init__(self):
        self.opcodes = (Add, FAdd, Sub, FSub, Mul, FMul, UDiv, SDiv, FDiv)
        self.dirty_operations = ["fadd", "fsub", "fmul", "udiv", "sdiv", "fdiv"]
        self.types = {"IntegerTypeKind" : "int",
                      "FloatTypeKind" : "float",
                      "DoubleTypeKind" : "double",
                      "VectorTypeKind" : "int"}

# def use_def(module):
    def parse(self, ir_file, top_function):
        module = self._create_module(ir_file)
        df = pd.DataFrame()
        function = module.get_named_function(top_function)
        for bb in function.iter_basic_blocks():
            for instruction in bb.iter_instructions():
                instruction_opcode = instruction.instruction_opcode
                if instruction_opcode in self.opcodes:
                    row = self._generate_row(instruction)
                    df = df.append(row, ignore_index=True)

        return df

    def _get_data_typekind(self, instruction):
        operand = instruction.get_operand(0)
        operand_type = operand.type_of()
        if(operand_type.get_kind() == VectorTypeKind):
            return TypeKind[operand_type.struct_get_type_at_index(0).get_kind()]
        else:
            return TypeKind[operand_type.get_kind()]

    def _get_operand_vector_lenght(self, instruction):
        # When one of the operands is a vector, a number
        # of operations equal to the lenght of the vector
        # can be parallelized
        vector_length = 1
        for num_op in range(0, instruction.num_operands):
            operand = instruction.get_operand(num_op)
            if(operand.type_of().get_kind() == VectorTypeKind):
                vector_length = max(vector_length, operand.type_of().vector_size)
        return vector_length

    def _get_int_bit_size(self, instruction):

        operand_type = instruction.get_operand(0).type_of()
        bit_size = (operand_type.get_int_type_width()
                if operand_type.get_kind() == IntegerTypeKind
                else pd.np.NaN)
        return bit_size

    def _get_first_level_dependencies(self, instruction):
        """
        Returns the list of operands of the evalued instruction if
        such operands are themselves operations.
        """
        dependencies = []
        # Cycle through the operands to get their definitions
        for num_op in range(0, instruction.num_operands):
            operand = instruction.get_operand(num_op)
            # Filter only the operations of interest
            # that we defined earlier
            if operand.instruction_opcode in self.opcodes: 
                dependencies.append(operand)
        return dependencies

    def _create_module(self, ir_file):
        buffer = create_memory_buffer_with_contents_of_file(ir_file)
        context = get_global_context()
        module = context.parse_ir(buffer)
        return module

    def _generate_row(self, instruction):
        # Define Opcodes of interest for us
        instruction_opcode = instruction.instruction_opcode
        data_type = self.types.get(self._get_data_typekind(instruction))
        # If the operation involves vectors, get their length
        vector_length = self._get_operand_vector_lenght(instruction)
        # If it is an integer operation, get the operands bitsize 
        int_bitsize = self._get_int_bit_size(instruction)
        # Get the list of dependencies of the current instruction
        dependencies = self._get_first_level_dependencies(instruction)

        # Create the DataFrame row using Series
        row = pd.Series()
        row['id_string'] = instruction.print_value_to_string()
        row['operation'] = self._sanitize(Opcode[instruction_opcode].lower())
        row['datatype'] = data_type
        row['vector_length'] = vector_length
        row['int_bitsize'] = int_bitsize
        row['dependencies'] = dependencies
        return row

    def _sanitize(self, operation):
        if operation in self.dirty_operations:
            return operation[1:]
        else:
            return operation
