; ModuleID = 'jacobi-2d.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@stderr = external global %struct._IO_FILE*, align 8
@.str = private unnamed_addr constant [23 x i8] c"==BEGIN DUMP_ARRAYS==\0A\00", align 1
@.str.1 = private unnamed_addr constant [15 x i8] c"begin dump: %s\00", align 1
@.str.2 = private unnamed_addr constant [2 x i8] c"A\00", align 1
@.str.4 = private unnamed_addr constant [8 x i8] c"%0.2lf \00", align 1
@.str.5 = private unnamed_addr constant [17 x i8] c"\0Aend   dump: %s\0A\00", align 1
@.str.6 = private unnamed_addr constant [23 x i8] c"==END   DUMP_ARRAYS==\0A\00", align 1

; Function Attrs: norecurse nounwind uwtable
define void @init_array(i32 %n, [1300 x double]* nocapture %A, [1300 x double]* nocapture %B) #0 {
  %1 = icmp sgt i32 %n, 0
  br i1 %1, label %.preheader.lr.ph, label %._crit_edge4

.preheader.lr.ph:                                 ; preds = %0
  %2 = sitofp i32 %n to double
  br label %.lr.ph.us

; <label>:3                                       ; preds = %3, %.lr.ph.us
  %indvars.iv = phi i64 [ 0, %.lr.ph.us ], [ %indvars.iv.next, %3 ]
  %4 = add nuw nsw i64 %indvars.iv, 2
  %5 = trunc i64 %4 to i32
  %6 = sitofp i32 %5 to double
  %7 = fmul double %19, %6
  %8 = fadd double %7, 2.000000e+00
  %9 = fdiv double %8, %2
  %10 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv5, i64 %indvars.iv
  store double %9, double* %10, align 8, !tbaa !1
  %11 = add nuw nsw i64 %indvars.iv, 3
  %12 = trunc i64 %11 to i32
  %13 = sitofp i32 %12 to double
  %14 = fmul double %19, %13
  %15 = fadd double %14, 3.000000e+00
  %16 = fdiv double %15, %2
  %17 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv5, i64 %indvars.iv
  store double %16, double* %17, align 8, !tbaa !1
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %lftr.wideiv = trunc i64 %indvars.iv.next to i32
  %exitcond = icmp eq i32 %lftr.wideiv, %n
  br i1 %exitcond, label %._crit_edge.us, label %3

.lr.ph.us:                                        ; preds = %._crit_edge.us, %.preheader.lr.ph
  %indvars.iv5 = phi i64 [ 0, %.preheader.lr.ph ], [ %indvars.iv.next6, %._crit_edge.us ]
  %18 = trunc i64 %indvars.iv5 to i32
  %19 = sitofp i32 %18 to double
  br label %3

._crit_edge.us:                                   ; preds = %3
  %indvars.iv.next6 = add nuw nsw i64 %indvars.iv5, 1
  %lftr.wideiv7 = trunc i64 %indvars.iv.next6 to i32
  %exitcond8 = icmp eq i32 %lftr.wideiv7, %n
  br i1 %exitcond8, label %._crit_edge4.loopexit, label %.lr.ph.us

._crit_edge4.loopexit:                            ; preds = %._crit_edge.us
  br label %._crit_edge4

._crit_edge4:                                     ; preds = %._crit_edge4.loopexit, %0
  ret void
}

; Function Attrs: nounwind uwtable
define void @print_array(i32 %n, [1300 x double]* nocapture readonly %A) #1 {
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !5
  %2 = tail call i64 @fwrite(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i64 0, i64 0), i64 22, i64 1, %struct._IO_FILE* %1) #5
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !5
  %4 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i64 0, i64 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i64 0, i64 0)) #5
  %5 = icmp sgt i32 %n, 0
  br i1 %5, label %.lr.ph.us.preheader, label %._crit_edge4

.lr.ph.us.preheader:                              ; preds = %0
  %6 = sext i32 %n to i64
  br label %.lr.ph.us

; <label>:7                                       ; preds = %14, %.lr.ph.us
  %indvars.iv = phi i64 [ 0, %.lr.ph.us ], [ %indvars.iv.next, %14 ]
  %8 = add nsw i64 %indvars.iv, %19
  %9 = trunc i64 %8 to i32
  %10 = srem i32 %9, 20
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %12, label %14

; <label>:12                                      ; preds = %7
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !5
  %fputc.us = tail call i32 @fputc(i32 10, %struct._IO_FILE* %13) #5
  br label %14

; <label>:14                                      ; preds = %12, %7
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !5
  %16 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv5, i64 %indvars.iv
  %17 = load double, double* %16, align 8, !tbaa !1
  %18 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* nonnull getelementptr inbounds ([8 x i8], [8 x i8]* @.str.4, i64 0, i64 0), double %17) #5
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %lftr.wideiv = trunc i64 %indvars.iv.next to i32
  %exitcond = icmp eq i32 %lftr.wideiv, %n
  br i1 %exitcond, label %._crit_edge.us, label %7

.lr.ph.us:                                        ; preds = %._crit_edge.us, %.lr.ph.us.preheader
  %indvars.iv5 = phi i64 [ 0, %.lr.ph.us.preheader ], [ %indvars.iv.next6, %._crit_edge.us ]
  %19 = mul nsw i64 %indvars.iv5, %6
  br label %7

._crit_edge.us:                                   ; preds = %14
  %indvars.iv.next6 = add nuw nsw i64 %indvars.iv5, 1
  %lftr.wideiv7 = trunc i64 %indvars.iv.next6 to i32
  %exitcond8 = icmp eq i32 %lftr.wideiv7, %n
  br i1 %exitcond8, label %._crit_edge4.loopexit, label %.lr.ph.us

._crit_edge4.loopexit:                            ; preds = %._crit_edge.us
  br label %._crit_edge4

._crit_edge4:                                     ; preds = %._crit_edge4.loopexit, %0
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !5
  %21 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %20, i8* nonnull getelementptr inbounds ([17 x i8], [17 x i8]* @.str.5, i64 0, i64 0), i8* nonnull getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i64 0, i64 0)) #5
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !5
  %23 = tail call i64 @fwrite(i8* nonnull getelementptr inbounds ([23 x i8], [23 x i8]* @.str.6, i64 0, i64 0), i64 22, i64 1, %struct._IO_FILE* %22) #5
  ret void
}

; Function Attrs: nounwind
declare i32 @fprintf(%struct._IO_FILE* nocapture, i8* nocapture readonly, ...) #2

; Function Attrs: norecurse nounwind uwtable
define void @kernel_jacobi_2d(i32 %tsteps, i32 %n, [1300 x double]* nocapture %A, [1300 x double]* nocapture %B, i32 %t, i32 %i, i32 %j) #0 {
  br label %.preheader6

.preheader6:                                      ; preds = %113, %0
  %.0311 = phi i32 [ 0, %0 ], [ %114, %113 ]
  br label %.preheader4

.preheader4:                                      ; preds = %58, %.preheader6
  %indvar37 = phi i64 [ %indvar.next38, %58 ], [ 0, %.preheader6 ]
  %indvars.iv12 = phi i64 [ %indvars.iv.next13, %58 ], [ 1, %.preheader6 ]
  %1 = add i64 %indvar37, 1
  %scevgep39 = getelementptr [1300 x double], [1300 x double]* %B, i64 %1, i64 1
  %scevgep41 = getelementptr [1300 x double], [1300 x double]* %B, i64 %1, i64 1298
  %scevgep43 = getelementptr [1300 x double], [1300 x double]* %A, i64 %indvar37, i64 1
  %2 = add i64 %indvar37, 2
  %scevgep45 = getelementptr [1300 x double], [1300 x double]* %A, i64 %2, i64 1298
  %indvars.iv.next13 = add nuw nsw i64 %indvars.iv12, 1
  %3 = add nsw i64 %indvars.iv12, -1
  %bound047 = icmp ule double* %scevgep39, %scevgep45
  %bound148 = icmp ule double* %scevgep43, %scevgep41
  %memcheck.conflict50 = and i1 %bound047, %bound148
  br i1 %memcheck.conflict50, label %scalar.ph34.preheader, label %vector.body32.preheader

vector.body32.preheader:                          ; preds = %.preheader4
  br label %vector.body32

vector.body32:                                    ; preds = %vector.body32.preheader, %vector.body32
  %index53 = phi i64 [ %index.next54, %vector.body32 ], [ 0, %vector.body32.preheader ]
  %offset.idx57 = or i64 %index53, 1
  %4 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv12, i64 %offset.idx57
  %5 = bitcast double* %4 to <2 x double>*
  %wide.load62 = load <2 x double>, <2 x double>* %5, align 8, !tbaa !1
  %6 = getelementptr double, double* %4, i64 2
  %7 = bitcast double* %6 to <2 x double>*
  %wide.load63 = load <2 x double>, <2 x double>* %7, align 8, !tbaa !1
  %8 = add i64 %offset.idx57, -1
  %9 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv12, i64 %8
  %10 = bitcast double* %9 to <2 x double>*
  %wide.load64 = load <2 x double>, <2 x double>* %10, align 8, !tbaa !1
  %11 = getelementptr double, double* %9, i64 2
  %12 = bitcast double* %11 to <2 x double>*
  %wide.load65 = load <2 x double>, <2 x double>* %12, align 8, !tbaa !1
  %13 = fadd <2 x double> %wide.load62, %wide.load64
  %14 = fadd <2 x double> %wide.load63, %wide.load65
  %15 = add nsw i64 %offset.idx57, 1
  %16 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv12, i64 %15
  %17 = bitcast double* %16 to <2 x double>*
  %wide.load66 = load <2 x double>, <2 x double>* %17, align 8, !tbaa !1
  %18 = getelementptr double, double* %16, i64 2
  %19 = bitcast double* %18 to <2 x double>*
  %wide.load67 = load <2 x double>, <2 x double>* %19, align 8, !tbaa !1
  %20 = fadd <2 x double> %13, %wide.load66
  %21 = fadd <2 x double> %14, %wide.load67
  %22 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv.next13, i64 %offset.idx57
  %23 = bitcast double* %22 to <2 x double>*
  %wide.load68 = load <2 x double>, <2 x double>* %23, align 8, !tbaa !1
  %24 = getelementptr double, double* %22, i64 2
  %25 = bitcast double* %24 to <2 x double>*
  %wide.load69 = load <2 x double>, <2 x double>* %25, align 8, !tbaa !1
  %26 = fadd <2 x double> %20, %wide.load68
  %27 = fadd <2 x double> %21, %wide.load69
  %28 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %3, i64 %offset.idx57
  %29 = bitcast double* %28 to <2 x double>*
  %wide.load70 = load <2 x double>, <2 x double>* %29, align 8, !tbaa !1
  %30 = getelementptr double, double* %28, i64 2
  %31 = bitcast double* %30 to <2 x double>*
  %wide.load71 = load <2 x double>, <2 x double>* %31, align 8, !tbaa !1
  %32 = fadd <2 x double> %26, %wide.load70
  %33 = fadd <2 x double> %27, %wide.load71
  %34 = fmul <2 x double> %32, <double 2.000000e-01, double 2.000000e-01>
  %35 = fmul <2 x double> %33, <double 2.000000e-01, double 2.000000e-01>
  %36 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv12, i64 %offset.idx57
  %37 = bitcast double* %36 to <2 x double>*
  store <2 x double> %34, <2 x double>* %37, align 8, !tbaa !1
  %38 = getelementptr double, double* %36, i64 2
  %39 = bitcast double* %38 to <2 x double>*
  store <2 x double> %35, <2 x double>* %39, align 8, !tbaa !1
  %index.next54 = add i64 %index53, 4
  %40 = icmp eq i64 %index.next54, 1296
  br i1 %40, label %scalar.ph34.preheader.loopexit, label %vector.body32, !llvm.loop !7

scalar.ph34.preheader.loopexit:                   ; preds = %vector.body32
  br label %scalar.ph34.preheader

scalar.ph34.preheader:                            ; preds = %scalar.ph34.preheader.loopexit, %.preheader4
  %indvars.iv.ph = phi i64 [ 1, %.preheader4 ], [ 1297, %scalar.ph34.preheader.loopexit ]
  br label %scalar.ph34

scalar.ph34:                                      ; preds = %scalar.ph34.preheader, %scalar.ph34
  %indvars.iv = phi i64 [ %indvars.iv.next, %scalar.ph34 ], [ %indvars.iv.ph, %scalar.ph34.preheader ]
  %41 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv12, i64 %indvars.iv
  %42 = load double, double* %41, align 8, !tbaa !1
  %43 = add nsw i64 %indvars.iv, -1
  %44 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv12, i64 %43
  %45 = load double, double* %44, align 8, !tbaa !1
  %46 = fadd double %42, %45
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %47 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv12, i64 %indvars.iv.next
  %48 = load double, double* %47, align 8, !tbaa !1
  %49 = fadd double %46, %48
  %50 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv.next13, i64 %indvars.iv
  %51 = load double, double* %50, align 8, !tbaa !1
  %52 = fadd double %49, %51
  %53 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %3, i64 %indvars.iv
  %54 = load double, double* %53, align 8, !tbaa !1
  %55 = fadd double %52, %54
  %56 = fmul double %55, 2.000000e-01
  %57 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv12, i64 %indvars.iv
  store double %56, double* %57, align 8, !tbaa !1
  %exitcond = icmp eq i64 %indvars.iv.next, 1299
  br i1 %exitcond, label %58, label %scalar.ph34, !llvm.loop !10

; <label>:58                                      ; preds = %scalar.ph34
  %exitcond14 = icmp eq i64 %indvars.iv.next13, 1299
  %indvar.next38 = add i64 %indvar37, 1
  br i1 %exitcond14, label %.preheader.preheader, label %.preheader4

.preheader.preheader:                             ; preds = %58
  br label %.preheader

.preheader:                                       ; preds = %.preheader.preheader, %112
  %indvar = phi i64 [ %indvar.next, %112 ], [ 0, %.preheader.preheader ]
  %indvars.iv18 = phi i64 [ %indvars.iv.next19, %112 ], [ 1, %.preheader.preheader ]
  %59 = add i64 %indvar, 1
  %scevgep = getelementptr [1300 x double], [1300 x double]* %A, i64 %59, i64 1
  %scevgep24 = getelementptr [1300 x double], [1300 x double]* %A, i64 %59, i64 1298
  %scevgep26 = getelementptr [1300 x double], [1300 x double]* %B, i64 %59, i64 1
  %scevgep28 = getelementptr [1300 x double], [1300 x double]* %B, i64 %59, i64 1298
  %bound0 = icmp ule double* %scevgep, %scevgep28
  %bound1 = icmp ule double* %scevgep26, %scevgep24
  %memcheck.conflict = and i1 %bound0, %bound1
  br i1 %memcheck.conflict, label %scalar.ph.preheader, label %vector.body.preheader

vector.body.preheader:                            ; preds = %.preheader
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.body.preheader
  %index = phi i64 [ 0, %vector.body.preheader ], [ %index.next.2, %vector.body ]
  %offset.idx = or i64 %index, 1
  %60 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv18, i64 %offset.idx
  %61 = bitcast double* %60 to <2 x i64>*
  %wide.load = load <2 x i64>, <2 x i64>* %61, align 8, !tbaa !1
  %62 = getelementptr double, double* %60, i64 2
  %63 = bitcast double* %62 to <2 x i64>*
  %wide.load31 = load <2 x i64>, <2 x i64>* %63, align 8, !tbaa !1
  %64 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv18, i64 %offset.idx
  %65 = bitcast double* %64 to <2 x i64>*
  store <2 x i64> %wide.load, <2 x i64>* %65, align 8, !tbaa !1
  %66 = getelementptr double, double* %64, i64 2
  %67 = bitcast double* %66 to <2 x i64>*
  store <2 x i64> %wide.load31, <2 x i64>* %67, align 8, !tbaa !1
  %index.next = add nuw nsw i64 %index, 4
  %offset.idx.1 = or i64 %index.next, 1
  %68 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv18, i64 %offset.idx.1
  %69 = bitcast double* %68 to <2 x i64>*
  %wide.load.1 = load <2 x i64>, <2 x i64>* %69, align 8, !tbaa !1
  %70 = getelementptr double, double* %68, i64 2
  %71 = bitcast double* %70 to <2 x i64>*
  %wide.load31.1 = load <2 x i64>, <2 x i64>* %71, align 8, !tbaa !1
  %72 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv18, i64 %offset.idx.1
  %73 = bitcast double* %72 to <2 x i64>*
  store <2 x i64> %wide.load.1, <2 x i64>* %73, align 8, !tbaa !1
  %74 = getelementptr double, double* %72, i64 2
  %75 = bitcast double* %74 to <2 x i64>*
  store <2 x i64> %wide.load31.1, <2 x i64>* %75, align 8, !tbaa !1
  %index.next.1 = add nsw i64 %index, 8
  %offset.idx.2 = or i64 %index.next.1, 1
  %76 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv18, i64 %offset.idx.2
  %77 = bitcast double* %76 to <2 x i64>*
  %wide.load.2 = load <2 x i64>, <2 x i64>* %77, align 8, !tbaa !1
  %78 = getelementptr double, double* %76, i64 2
  %79 = bitcast double* %78 to <2 x i64>*
  %wide.load31.2 = load <2 x i64>, <2 x i64>* %79, align 8, !tbaa !1
  %80 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv18, i64 %offset.idx.2
  %81 = bitcast double* %80 to <2 x i64>*
  store <2 x i64> %wide.load.2, <2 x i64>* %81, align 8, !tbaa !1
  %82 = getelementptr double, double* %80, i64 2
  %83 = bitcast double* %82 to <2 x i64>*
  store <2 x i64> %wide.load31.2, <2 x i64>* %83, align 8, !tbaa !1
  %index.next.2 = add nsw i64 %index, 12
  %84 = icmp eq i64 %index.next.2, 1296
  br i1 %84, label %scalar.ph.preheader.loopexit, label %vector.body, !llvm.loop !11

scalar.ph.preheader.loopexit:                     ; preds = %vector.body
  br label %scalar.ph.preheader

scalar.ph.preheader:                              ; preds = %scalar.ph.preheader.loopexit, %.preheader
  %indvars.iv15.ph = phi i64 [ 1, %.preheader ], [ 1297, %scalar.ph.preheader.loopexit ]
  %85 = sub nsw i64 1298, %indvars.iv15.ph
  br i1 true, label %scalar.ph.prol.preheader, label %scalar.ph.preheader.split

scalar.ph.prol.preheader:                         ; preds = %scalar.ph.preheader
  br label %scalar.ph.prol

scalar.ph.prol:                                   ; preds = %scalar.ph.prol, %scalar.ph.prol.preheader
  %indvars.iv15.prol = phi i64 [ %indvars.iv.next16.prol, %scalar.ph.prol ], [ %indvars.iv15.ph, %scalar.ph.prol.preheader ]
  %prol.iter = phi i64 [ %prol.iter.sub, %scalar.ph.prol ], [ 2, %scalar.ph.prol.preheader ]
  %86 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv18, i64 %indvars.iv15.prol
  %87 = bitcast double* %86 to i64*
  %88 = load i64, i64* %87, align 8, !tbaa !1
  %89 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv18, i64 %indvars.iv15.prol
  %90 = bitcast double* %89 to i64*
  store i64 %88, i64* %90, align 8, !tbaa !1
  %indvars.iv.next16.prol = add nuw nsw i64 %indvars.iv15.prol, 1
  %prol.iter.sub = add i64 %prol.iter, -1
  %prol.iter.cmp = icmp eq i64 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %scalar.ph.preheader.split.loopexit, label %scalar.ph.prol, !llvm.loop !12

scalar.ph.preheader.split.loopexit:               ; preds = %scalar.ph.prol
  %indvars.iv.next16.prol.lcssa = phi i64 [ %indvars.iv.next16.prol, %scalar.ph.prol ]
  br label %scalar.ph.preheader.split

scalar.ph.preheader.split:                        ; preds = %scalar.ph.preheader.split.loopexit, %scalar.ph.preheader
  %indvars.iv15.unr = phi i64 [ %indvars.iv15.ph, %scalar.ph.preheader ], [ %indvars.iv.next16.prol.lcssa, %scalar.ph.preheader.split.loopexit ]
  %91 = icmp ult i64 %85, 3
  br i1 %91, label %112, label %scalar.ph.preheader.split.split

scalar.ph.preheader.split.split:                  ; preds = %scalar.ph.preheader.split
  br label %scalar.ph

scalar.ph:                                        ; preds = %scalar.ph, %scalar.ph.preheader.split.split
  %indvars.iv15 = phi i64 [ %indvars.iv15.unr, %scalar.ph.preheader.split.split ], [ %indvars.iv.next16.3, %scalar.ph ]
  %92 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv18, i64 %indvars.iv15
  %93 = bitcast double* %92 to i64*
  %94 = load i64, i64* %93, align 8, !tbaa !1
  %95 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv18, i64 %indvars.iv15
  %96 = bitcast double* %95 to i64*
  store i64 %94, i64* %96, align 8, !tbaa !1
  %indvars.iv.next16 = add nuw nsw i64 %indvars.iv15, 1
  %97 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv18, i64 %indvars.iv.next16
  %98 = bitcast double* %97 to i64*
  %99 = load i64, i64* %98, align 8, !tbaa !1
  %100 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv18, i64 %indvars.iv.next16
  %101 = bitcast double* %100 to i64*
  store i64 %99, i64* %101, align 8, !tbaa !1
  %indvars.iv.next16.1 = add nsw i64 %indvars.iv15, 2
  %102 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv18, i64 %indvars.iv.next16.1
  %103 = bitcast double* %102 to i64*
  %104 = load i64, i64* %103, align 8, !tbaa !1
  %105 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv18, i64 %indvars.iv.next16.1
  %106 = bitcast double* %105 to i64*
  store i64 %104, i64* %106, align 8, !tbaa !1
  %indvars.iv.next16.2 = add nsw i64 %indvars.iv15, 3
  %107 = getelementptr inbounds [1300 x double], [1300 x double]* %B, i64 %indvars.iv18, i64 %indvars.iv.next16.2
  %108 = bitcast double* %107 to i64*
  %109 = load i64, i64* %108, align 8, !tbaa !1
  %110 = getelementptr inbounds [1300 x double], [1300 x double]* %A, i64 %indvars.iv18, i64 %indvars.iv.next16.2
  %111 = bitcast double* %110 to i64*
  store i64 %109, i64* %111, align 8, !tbaa !1
  %indvars.iv.next16.3 = add nsw i64 %indvars.iv15, 4
  %exitcond17.3 = icmp eq i64 %indvars.iv.next16.3, 1299
  br i1 %exitcond17.3, label %.unr-lcssa, label %scalar.ph, !llvm.loop !14

.unr-lcssa:                                       ; preds = %scalar.ph
  br label %112

; <label>:112                                     ; preds = %scalar.ph.preheader.split, %.unr-lcssa
  %indvars.iv.next19 = add nuw nsw i64 %indvars.iv18, 1
  %exitcond20 = icmp eq i64 %indvars.iv.next19, 1299
  %indvar.next = add i64 %indvar, 1
  br i1 %exitcond20, label %113, label %.preheader

; <label>:113                                     ; preds = %112
  %114 = add nuw nsw i32 %.0311, 1
  %exitcond21 = icmp eq i32 %114, 500
  br i1 %exitcond21, label %115, label %.preheader6

; <label>:115                                     ; preds = %113
  ret void
}

; Function Attrs: nounwind uwtable
define i32 @main(i32 %argc, i8** nocapture readonly %argv) #1 {
  %1 = tail call i8* @polybench_alloc_data(i64 1690000, i32 8) #4
  %2 = tail call i8* @polybench_alloc_data(i64 1690000, i32 8) #4
  %3 = bitcast i8* %1 to [1300 x double]*
  %4 = bitcast i8* %2 to [1300 x double]*
  br label %.lr.ph.us.i

; <label>:5                                       ; preds = %.lr.ph.us.i, %5
  %indvars.iv.i = phi i64 [ 0, %.lr.ph.us.i ], [ %indvars.iv.next.i, %5 ]
  %6 = add nuw nsw i64 %indvars.iv.i, 2
  %7 = trunc i64 %6 to i32
  %8 = sitofp i32 %7 to double
  %9 = fmul double %21, %8
  %10 = fadd double %9, 2.000000e+00
  %11 = fdiv double %10, 1.300000e+03
  %12 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv5.i, i64 %indvars.iv.i
  store double %11, double* %12, align 8, !tbaa !1
  %13 = add nuw nsw i64 %indvars.iv.i, 3
  %14 = trunc i64 %13 to i32
  %15 = sitofp i32 %14 to double
  %16 = fmul double %21, %15
  %17 = fadd double %16, 3.000000e+00
  %18 = fdiv double %17, 1.300000e+03
  %19 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv5.i, i64 %indvars.iv.i
  store double %18, double* %19, align 8, !tbaa !1
  %indvars.iv.next.i = add nuw nsw i64 %indvars.iv.i, 1
  %exitcond = icmp eq i64 %indvars.iv.next.i, 1300
  br i1 %exitcond, label %._crit_edge.us.i, label %5

.lr.ph.us.i:                                      ; preds = %._crit_edge.us.i, %0
  %indvars.iv5.i = phi i64 [ 0, %0 ], [ %indvars.iv.next6.i, %._crit_edge.us.i ]
  %20 = trunc i64 %indvars.iv5.i to i32
  %21 = sitofp i32 %20 to double
  br label %5

._crit_edge.us.i:                                 ; preds = %5
  %indvars.iv.next6.i = add nuw nsw i64 %indvars.iv5.i, 1
  %exitcond9 = icmp eq i64 %indvars.iv.next6.i, 1300
  br i1 %exitcond9, label %.preheader6.i.preheader, label %.lr.ph.us.i

.preheader6.i.preheader:                          ; preds = %._crit_edge.us.i
  br label %.preheader6.i

.preheader6.i:                                    ; preds = %.preheader6.i.preheader, %139
  %.0311.i = phi i32 [ %140, %139 ], [ 0, %.preheader6.i.preheader ]
  br label %.preheader4.i

.preheader4.i:                                    ; preds = %82, %.preheader6.i
  %indvar20 = phi i64 [ %indvar.next21, %82 ], [ 0, %.preheader6.i ]
  %indvars.iv12.i = phi i64 [ %indvars.iv.next13.i, %82 ], [ 1, %.preheader6.i ]
  %22 = mul i64 %indvar20, 10400
  %23 = add i64 %22, 10408
  %scevgep22 = getelementptr i8, i8* %2, i64 %23
  %24 = add i64 %22, 20784
  %scevgep23 = getelementptr i8, i8* %2, i64 %24
  %25 = or i64 %22, 8
  %scevgep24 = getelementptr i8, i8* %1, i64 %25
  %26 = add i64 %22, 31184
  %scevgep25 = getelementptr i8, i8* %1, i64 %26
  %indvars.iv.next13.i = add nuw nsw i64 %indvars.iv12.i, 1
  %27 = add nsw i64 %indvars.iv12.i, -1
  %bound026 = icmp ule i8* %scevgep22, %scevgep25
  %bound127 = icmp ule i8* %scevgep24, %scevgep23
  %memcheck.conflict29 = and i1 %bound026, %bound127
  br i1 %memcheck.conflict29, label %scalar.ph17.preheader, label %vector.body15.preheader

vector.body15.preheader:                          ; preds = %.preheader4.i
  br label %vector.body15

vector.body15:                                    ; preds = %vector.body15.preheader, %vector.body15
  %index32 = phi i64 [ %index.next33, %vector.body15 ], [ 0, %vector.body15.preheader ]
  %offset.idx36 = or i64 %index32, 1
  %28 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv12.i, i64 %offset.idx36
  %29 = bitcast double* %28 to <2 x double>*
  %wide.load41 = load <2 x double>, <2 x double>* %29, align 8, !tbaa !1
  %30 = getelementptr double, double* %28, i64 2
  %31 = bitcast double* %30 to <2 x double>*
  %wide.load42 = load <2 x double>, <2 x double>* %31, align 8, !tbaa !1
  %32 = add i64 %offset.idx36, -1
  %33 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv12.i, i64 %32
  %34 = bitcast double* %33 to <2 x double>*
  %wide.load43 = load <2 x double>, <2 x double>* %34, align 8, !tbaa !1
  %35 = getelementptr double, double* %33, i64 2
  %36 = bitcast double* %35 to <2 x double>*
  %wide.load44 = load <2 x double>, <2 x double>* %36, align 8, !tbaa !1
  %37 = fadd <2 x double> %wide.load41, %wide.load43
  %38 = fadd <2 x double> %wide.load42, %wide.load44
  %39 = add nsw i64 %offset.idx36, 1
  %40 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv12.i, i64 %39
  %41 = bitcast double* %40 to <2 x double>*
  %wide.load45 = load <2 x double>, <2 x double>* %41, align 8, !tbaa !1
  %42 = getelementptr double, double* %40, i64 2
  %43 = bitcast double* %42 to <2 x double>*
  %wide.load46 = load <2 x double>, <2 x double>* %43, align 8, !tbaa !1
  %44 = fadd <2 x double> %37, %wide.load45
  %45 = fadd <2 x double> %38, %wide.load46
  %46 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv.next13.i, i64 %offset.idx36
  %47 = bitcast double* %46 to <2 x double>*
  %wide.load47 = load <2 x double>, <2 x double>* %47, align 8, !tbaa !1
  %48 = getelementptr double, double* %46, i64 2
  %49 = bitcast double* %48 to <2 x double>*
  %wide.load48 = load <2 x double>, <2 x double>* %49, align 8, !tbaa !1
  %50 = fadd <2 x double> %44, %wide.load47
  %51 = fadd <2 x double> %45, %wide.load48
  %52 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %27, i64 %offset.idx36
  %53 = bitcast double* %52 to <2 x double>*
  %wide.load49 = load <2 x double>, <2 x double>* %53, align 8, !tbaa !1
  %54 = getelementptr double, double* %52, i64 2
  %55 = bitcast double* %54 to <2 x double>*
  %wide.load50 = load <2 x double>, <2 x double>* %55, align 8, !tbaa !1
  %56 = fadd <2 x double> %50, %wide.load49
  %57 = fadd <2 x double> %51, %wide.load50
  %58 = fmul <2 x double> %56, <double 2.000000e-01, double 2.000000e-01>
  %59 = fmul <2 x double> %57, <double 2.000000e-01, double 2.000000e-01>
  %60 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv12.i, i64 %offset.idx36
  %61 = bitcast double* %60 to <2 x double>*
  store <2 x double> %58, <2 x double>* %61, align 8, !tbaa !1
  %62 = getelementptr double, double* %60, i64 2
  %63 = bitcast double* %62 to <2 x double>*
  store <2 x double> %59, <2 x double>* %63, align 8, !tbaa !1
  %index.next33 = add i64 %index32, 4
  %64 = icmp eq i64 %index.next33, 1296
  br i1 %64, label %scalar.ph17.preheader.loopexit, label %vector.body15, !llvm.loop !15

scalar.ph17.preheader.loopexit:                   ; preds = %vector.body15
  br label %scalar.ph17.preheader

scalar.ph17.preheader:                            ; preds = %scalar.ph17.preheader.loopexit, %.preheader4.i
  %indvars.iv.i6.ph = phi i64 [ 1, %.preheader4.i ], [ 1297, %scalar.ph17.preheader.loopexit ]
  br label %scalar.ph17

scalar.ph17:                                      ; preds = %scalar.ph17.preheader, %scalar.ph17
  %indvars.iv.i6 = phi i64 [ %indvars.iv.next.i7, %scalar.ph17 ], [ %indvars.iv.i6.ph, %scalar.ph17.preheader ]
  %65 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv12.i, i64 %indvars.iv.i6
  %66 = load double, double* %65, align 8, !tbaa !1
  %67 = add nsw i64 %indvars.iv.i6, -1
  %68 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv12.i, i64 %67
  %69 = load double, double* %68, align 8, !tbaa !1
  %70 = fadd double %66, %69
  %indvars.iv.next.i7 = add nuw nsw i64 %indvars.iv.i6, 1
  %71 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv12.i, i64 %indvars.iv.next.i7
  %72 = load double, double* %71, align 8, !tbaa !1
  %73 = fadd double %70, %72
  %74 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv.next13.i, i64 %indvars.iv.i6
  %75 = load double, double* %74, align 8, !tbaa !1
  %76 = fadd double %73, %75
  %77 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %27, i64 %indvars.iv.i6
  %78 = load double, double* %77, align 8, !tbaa !1
  %79 = fadd double %76, %78
  %80 = fmul double %79, 2.000000e-01
  %81 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv12.i, i64 %indvars.iv.i6
  store double %80, double* %81, align 8, !tbaa !1
  %exitcond.i8 = icmp eq i64 %indvars.iv.next.i7, 1299
  br i1 %exitcond.i8, label %82, label %scalar.ph17, !llvm.loop !16

; <label>:82                                      ; preds = %scalar.ph17
  %exitcond14.i = icmp eq i64 %indvars.iv.next13.i, 1299
  %indvar.next21 = add i64 %indvar20, 1
  br i1 %exitcond14.i, label %.preheader.i.preheader, label %.preheader4.i

.preheader.i.preheader:                           ; preds = %82
  br label %.preheader.i

.preheader.i:                                     ; preds = %.preheader.i.preheader, %138
  %indvar = phi i64 [ %indvar.next, %138 ], [ 0, %.preheader.i.preheader ]
  %indvars.iv18.i = phi i64 [ %indvars.iv.next19.i, %138 ], [ 1, %.preheader.i.preheader ]
  %83 = mul i64 %indvar, 10400
  %84 = add i64 %83, 10408
  %scevgep = getelementptr i8, i8* %1, i64 %84
  %85 = add i64 %83, 20784
  %scevgep10 = getelementptr i8, i8* %1, i64 %85
  %scevgep11 = getelementptr i8, i8* %2, i64 %84
  %scevgep12 = getelementptr i8, i8* %2, i64 %85
  %bound0 = icmp ule i8* %scevgep, %scevgep12
  %bound1 = icmp ule i8* %scevgep11, %scevgep10
  %memcheck.conflict = and i1 %bound0, %bound1
  br i1 %memcheck.conflict, label %scalar.ph.preheader, label %vector.body.preheader

vector.body.preheader:                            ; preds = %.preheader.i
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.body.preheader
  %index = phi i64 [ 0, %vector.body.preheader ], [ %index.next.2, %vector.body ]
  %offset.idx = or i64 %index, 1
  %86 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv18.i, i64 %offset.idx
  %87 = bitcast double* %86 to <2 x i64>*
  %wide.load = load <2 x i64>, <2 x i64>* %87, align 8, !tbaa !1
  %88 = getelementptr double, double* %86, i64 2
  %89 = bitcast double* %88 to <2 x i64>*
  %wide.load14 = load <2 x i64>, <2 x i64>* %89, align 8, !tbaa !1
  %90 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv18.i, i64 %offset.idx
  %91 = bitcast double* %90 to <2 x i64>*
  store <2 x i64> %wide.load, <2 x i64>* %91, align 8, !tbaa !1
  %92 = getelementptr double, double* %90, i64 2
  %93 = bitcast double* %92 to <2 x i64>*
  store <2 x i64> %wide.load14, <2 x i64>* %93, align 8, !tbaa !1
  %index.next = add nuw nsw i64 %index, 4
  %offset.idx.1 = or i64 %index.next, 1
  %94 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv18.i, i64 %offset.idx.1
  %95 = bitcast double* %94 to <2 x i64>*
  %wide.load.1 = load <2 x i64>, <2 x i64>* %95, align 8, !tbaa !1
  %96 = getelementptr double, double* %94, i64 2
  %97 = bitcast double* %96 to <2 x i64>*
  %wide.load14.1 = load <2 x i64>, <2 x i64>* %97, align 8, !tbaa !1
  %98 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv18.i, i64 %offset.idx.1
  %99 = bitcast double* %98 to <2 x i64>*
  store <2 x i64> %wide.load.1, <2 x i64>* %99, align 8, !tbaa !1
  %100 = getelementptr double, double* %98, i64 2
  %101 = bitcast double* %100 to <2 x i64>*
  store <2 x i64> %wide.load14.1, <2 x i64>* %101, align 8, !tbaa !1
  %index.next.1 = add nsw i64 %index, 8
  %offset.idx.2 = or i64 %index.next.1, 1
  %102 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv18.i, i64 %offset.idx.2
  %103 = bitcast double* %102 to <2 x i64>*
  %wide.load.2 = load <2 x i64>, <2 x i64>* %103, align 8, !tbaa !1
  %104 = getelementptr double, double* %102, i64 2
  %105 = bitcast double* %104 to <2 x i64>*
  %wide.load14.2 = load <2 x i64>, <2 x i64>* %105, align 8, !tbaa !1
  %106 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv18.i, i64 %offset.idx.2
  %107 = bitcast double* %106 to <2 x i64>*
  store <2 x i64> %wide.load.2, <2 x i64>* %107, align 8, !tbaa !1
  %108 = getelementptr double, double* %106, i64 2
  %109 = bitcast double* %108 to <2 x i64>*
  store <2 x i64> %wide.load14.2, <2 x i64>* %109, align 8, !tbaa !1
  %index.next.2 = add nsw i64 %index, 12
  %110 = icmp eq i64 %index.next.2, 1296
  br i1 %110, label %scalar.ph.preheader.loopexit, label %vector.body, !llvm.loop !17

scalar.ph.preheader.loopexit:                     ; preds = %vector.body
  br label %scalar.ph.preheader

scalar.ph.preheader:                              ; preds = %scalar.ph.preheader.loopexit, %.preheader.i
  %indvars.iv15.i.ph = phi i64 [ 1, %.preheader.i ], [ 1297, %scalar.ph.preheader.loopexit ]
  %111 = sub nsw i64 1298, %indvars.iv15.i.ph
  br i1 true, label %scalar.ph.prol.preheader, label %scalar.ph.preheader.split

scalar.ph.prol.preheader:                         ; preds = %scalar.ph.preheader
  br label %scalar.ph.prol

scalar.ph.prol:                                   ; preds = %scalar.ph.prol, %scalar.ph.prol.preheader
  %indvars.iv15.i.prol = phi i64 [ %indvars.iv.next16.i.prol, %scalar.ph.prol ], [ %indvars.iv15.i.ph, %scalar.ph.prol.preheader ]
  %prol.iter = phi i64 [ %prol.iter.sub, %scalar.ph.prol ], [ 2, %scalar.ph.prol.preheader ]
  %112 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv18.i, i64 %indvars.iv15.i.prol
  %113 = bitcast double* %112 to i64*
  %114 = load i64, i64* %113, align 8, !tbaa !1
  %115 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv18.i, i64 %indvars.iv15.i.prol
  %116 = bitcast double* %115 to i64*
  store i64 %114, i64* %116, align 8, !tbaa !1
  %indvars.iv.next16.i.prol = add nuw nsw i64 %indvars.iv15.i.prol, 1
  %prol.iter.sub = add i64 %prol.iter, -1
  %prol.iter.cmp = icmp eq i64 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %scalar.ph.preheader.split.loopexit, label %scalar.ph.prol, !llvm.loop !18

scalar.ph.preheader.split.loopexit:               ; preds = %scalar.ph.prol
  %indvars.iv.next16.i.prol.lcssa = phi i64 [ %indvars.iv.next16.i.prol, %scalar.ph.prol ]
  br label %scalar.ph.preheader.split

scalar.ph.preheader.split:                        ; preds = %scalar.ph.preheader.split.loopexit, %scalar.ph.preheader
  %indvars.iv15.i.unr = phi i64 [ %indvars.iv15.i.ph, %scalar.ph.preheader ], [ %indvars.iv.next16.i.prol.lcssa, %scalar.ph.preheader.split.loopexit ]
  %117 = icmp ult i64 %111, 3
  br i1 %117, label %138, label %scalar.ph.preheader.split.split

scalar.ph.preheader.split.split:                  ; preds = %scalar.ph.preheader.split
  br label %scalar.ph

scalar.ph:                                        ; preds = %scalar.ph, %scalar.ph.preheader.split.split
  %indvars.iv15.i = phi i64 [ %indvars.iv15.i.unr, %scalar.ph.preheader.split.split ], [ %indvars.iv.next16.i.3, %scalar.ph ]
  %118 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv18.i, i64 %indvars.iv15.i
  %119 = bitcast double* %118 to i64*
  %120 = load i64, i64* %119, align 8, !tbaa !1
  %121 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv18.i, i64 %indvars.iv15.i
  %122 = bitcast double* %121 to i64*
  store i64 %120, i64* %122, align 8, !tbaa !1
  %indvars.iv.next16.i = add nuw nsw i64 %indvars.iv15.i, 1
  %123 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv18.i, i64 %indvars.iv.next16.i
  %124 = bitcast double* %123 to i64*
  %125 = load i64, i64* %124, align 8, !tbaa !1
  %126 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv18.i, i64 %indvars.iv.next16.i
  %127 = bitcast double* %126 to i64*
  store i64 %125, i64* %127, align 8, !tbaa !1
  %indvars.iv.next16.i.1 = add nsw i64 %indvars.iv15.i, 2
  %128 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv18.i, i64 %indvars.iv.next16.i.1
  %129 = bitcast double* %128 to i64*
  %130 = load i64, i64* %129, align 8, !tbaa !1
  %131 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv18.i, i64 %indvars.iv.next16.i.1
  %132 = bitcast double* %131 to i64*
  store i64 %130, i64* %132, align 8, !tbaa !1
  %indvars.iv.next16.i.2 = add nsw i64 %indvars.iv15.i, 3
  %133 = getelementptr inbounds [1300 x double], [1300 x double]* %4, i64 %indvars.iv18.i, i64 %indvars.iv.next16.i.2
  %134 = bitcast double* %133 to i64*
  %135 = load i64, i64* %134, align 8, !tbaa !1
  %136 = getelementptr inbounds [1300 x double], [1300 x double]* %3, i64 %indvars.iv18.i, i64 %indvars.iv.next16.i.2
  %137 = bitcast double* %136 to i64*
  store i64 %135, i64* %137, align 8, !tbaa !1
  %indvars.iv.next16.i.3 = add nsw i64 %indvars.iv15.i, 4
  %exitcond17.i.3 = icmp eq i64 %indvars.iv.next16.i.3, 1299
  br i1 %exitcond17.i.3, label %.unr-lcssa, label %scalar.ph, !llvm.loop !19

.unr-lcssa:                                       ; preds = %scalar.ph
  br label %138

; <label>:138                                     ; preds = %scalar.ph.preheader.split, %.unr-lcssa
  %indvars.iv.next19.i = add nuw nsw i64 %indvars.iv18.i, 1
  %exitcond20.i = icmp eq i64 %indvars.iv.next19.i, 1299
  %indvar.next = add i64 %indvar, 1
  br i1 %exitcond20.i, label %139, label %.preheader.i

; <label>:139                                     ; preds = %138
  %140 = add nuw nsw i32 %.0311.i, 1
  %exitcond21.i = icmp eq i32 %140, 500
  br i1 %exitcond21.i, label %kernel_jacobi_2d.exit, label %.preheader6.i

kernel_jacobi_2d.exit:                            ; preds = %139
  %141 = icmp sgt i32 %argc, 42
  br i1 %141, label %142, label %146

; <label>:142                                     ; preds = %kernel_jacobi_2d.exit
  %143 = load i8*, i8** %argv, align 8, !tbaa !5
  %144 = load i8, i8* %143, align 1, !tbaa !20
  %phitmp = icmp eq i8 %144, 0
  br i1 %phitmp, label %145, label %146

; <label>:145                                     ; preds = %142
  tail call void @print_array(i32 1300, [1300 x double]* nonnull %3)
  br label %146

; <label>:146                                     ; preds = %142, %145, %kernel_jacobi_2d.exit
  tail call void @free(i8* nonnull %1) #4
  tail call void @free(i8* nonnull %2) #4
  ret i32 0
}

declare i8* @polybench_alloc_data(i64, i32) #3

; Function Attrs: nounwind
declare void @free(i8* nocapture) #2

; Function Attrs: nounwind
declare i64 @fwrite(i8* nocapture, i64, i64, %struct._IO_FILE* nocapture) #4

; Function Attrs: nounwind
declare i32 @fputc(i32, %struct._IO_FILE* nocapture) #4

attributes #0 = { norecurse nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { cold }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.8.0-2ubuntu4 (tags/RELEASE_380/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"double", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
!7 = distinct !{!7, !8, !9}
!8 = !{!"llvm.loop.vectorize.width", i32 1}
!9 = !{!"llvm.loop.interleave.count", i32 1}
!10 = distinct !{!10, !8, !9}
!11 = distinct !{!11, !8, !9}
!12 = distinct !{!12, !13}
!13 = !{!"llvm.loop.unroll.disable"}
!14 = distinct !{!14, !8, !9}
!15 = distinct !{!15, !8, !9}
!16 = distinct !{!16, !8, !9}
!17 = distinct !{!17, !8, !9}
!18 = distinct !{!18, !13}
!19 = distinct !{!19, !8, !9}
!20 = !{!3, !3, i64 0}
