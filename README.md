# Sibyl
---
Sibyl is a *fast and accurate hardware resources estimator* that is able to estimate the usage of hardware resources required to implement and algorithm on FPGA

### Installation and Usage
**N.B.** This instructions has been tested on **Ubuntu 16.04.2 LTS** using `llvm 3.8.0` and `python 3.5.2`

In order to run Sibyl we need to install `llvm` and `python3`. We will also install `pip` and `virtualenv` to easly manage its dependencies.
```
sudo apt-get install python3 python3-virtualenv virtualenv python3-pip llvm
```
Clone the repository from BitBucket
```
git clone https://cippaciong@bitbucket.org/necst/xohw17_sibyl_public.git
```
Enter in the repository directory and create a new directory for the virtualenv
```
cd xohw17_sibyl_public/sibyl && mkdir sibyl_venv
```
Create the virtualenv using python3 as interpreter
```
virtualenv -p /usr/bin/python3 sibyl_venv
```
Activate the virtualenv
```
source sibyl_venv/bin/activate
```
Install the packages required to run Sibyl
```
pip install click pandas git+https://github.com/revng/llvmcpy.git
```
Finally you can run Sibyl on one of the LLVM IR samples provided in this way
```
python sibyl.py --llvm-ir test/jacobi-2d.ll --top-function kernel_jacobi_2d
```

### Generate the LLVM IR
In the `test` folder inside the git repo of Sibyl there are already two LLVM IR files for two different algorithm you can use for testing.
If you want to regenerate them using a different llvm version, or if you would like to try Sibyl on another algorithm, these are the steps required to generate a new `.ll` file.
```
sudo apt-get install clang
cd test && rm jacobi-2d.ll
clang -S -emit-llvm -O3 -o jacobi-2d.ll jacobi-2d.c
```
